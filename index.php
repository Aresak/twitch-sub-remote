<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 23.02.2017
 * Time: 20:33
 */
include "twitchtv.php";
include "twitchsub.php";
echo "<script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>";

error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_GET)){
    if(isset($_GET["clear"])) {
        session_destroy();
		header('Location: index.php');
    }
}


$twitch = new TwitchTV();

if(isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
    $name = $twitch->authenticated_user($token);
    $sqlname = mysqli_escape_string($sql, $name);

    $ch = curl_init("https://api.twitch.tv/kraken/users/" . $name . "/subscriptions/" . $stream);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: OAuth ' . $token
    ));
    $output = curl_exec($ch);
    curl_close($ch);

    $updated = explode(";", file_get_contents("updated"));

    $json = json_decode($output, true);

    $params = array("twitch_name" => $name, "whitelisted_list" => drawTable($sql));
    if(isset($json["error"])) {
        drawPage2("pleb.html", $params);

        $search_q = "SELECT * FROM ts_links WHERE twitch='$sqlname'";
        $search_r = mysqli_query($sql, $search_q)
        or die(mysqli_error($sql));
        if(mysqli_num_rows($search_r) > 0) {
            $update_q = "UPDATE ts_links SET is_sub='0' WHERE twitch='$sqlname'";
            $update_r = mysqli_query($sql, $update_q)
            or die(mysqli_error($sql));
        }
    } else {
        $showform = true;
        $canplay = true;
        if(isset($_POST)) {
            if(isset($_POST["mcname"])) {
                $sqlmc = mysqli_escape_string($sql, $_POST["mcname"]);
                $sqlsteam = mysqli_escape_string($sql, $_POST["steam"]);
                $query = "SELECT * FROM ts_links WHERE twitch='$sqlname'";
                $result = mysqli_query($sql, $query)
                or die(mysqli_error($sql));

                if(mysqli_num_rows($result) > 0) {
                    $showform = false;
                } else {
                    $insert_query = "INSERT INTO ts_links (twitch, minecraft, is_sub, token, reg_IP, steam)
                    VALUES ('$name', '$sqlmc', '1', '$token', '" . $_SERVER["REMOTE_ADDR"] . "', '$sqlsteam')";
                    $insert_result = mysqli_query($sql, $insert_query)
                    or die(mysqli_error($sql));
                    $showform = false;

                    $nextUpdateID = $updated[1] ++;
                    $_SESSION["canPlay"] = $nextUpdateID;
                    $_SESSION["canPlayTime"] = $updated[0] + 300;
                    $canplay = false;
                }
            }
        }
        $params["mcname"] = "";
        $params["steamid"] = "";
        $params["input_mc"] = "";
        $params["input_steam"] = "";
        $params["input_form"] = "onClick='$(\"#subform\").submit()'";
        $params["banned"] = "";

        $userq = "SELECT * FROM ts_links WHERE twitch='$sqlname'";
        $userr = mysqli_query($sql, $userq)
        or die(mysqli_error($sql));

        if(mysqli_result($userr, 0, "is_sub") == 0) {
            $update_q = "UPDATE ts_links SET is_sub='1' WHERE ID='" . mysqli_result($userr, 0, "ID") . "'";
            $update_r = mysqli_query($sql, $update_q)
            or die(mysqli_error($sql));
        }

        if(mysqli_num_rows($userr) > 0) {
            $showform = false;
            $bannedq = "SELECT * FROM ts_bans WHERE link_id='" . mysqli_result($userr, 0, "ID")  . "' OR mcname='" . mysqli_result($userr, 0, "minecraft") . "'";
            $bannedr = mysqli_query($sql, $bannedq)
            or die(mysqli_error($sql));

            $params["mcname"] = mysqli_result($userr, 0, "minecraft");
            $params["steamid"] = mysqli_result($userr, 0, "steam");
            $params["input_mc"] = "value='" . $params["mcname"] . "' disabled";
            $params["input_steam"] = "value='" . $params["steamid"] . "' disabled";
            $params["input_form"] = "onClick='return;'";
            $params["mc_server_message"] = "<span class='green-text'>You can play!</span>";
            if(mysqli_num_rows($bannedr) > 0) {
                if(mysqli_result($bannedr, 0, "active") == 1) {
                    $canplay = false;
                    $params["mc_server_message"] = "<span class='red-text'>You can't play! Try again later :(</span>";

                    if(mysqli_result($bannedr, 0, "link_id") != mysqli_result($userr, 0, "ID")) {
                        $banupdate = "UPDATE ts_bans SET link_id='" . mysqli_result($userr, 0, "ID") . "' WHERE ID='" . mysqli_result($bannedr, 0, "ID") . "'";
                        $banupdater = mysqli_query($sql, $banupdate)
                        or die(mysqli_error($sql));
                    }

                    $banquery1 = "UPDATE ts_bans SET active='0' WHERE link_id='" . mysqli_result($userr, 0, "ID") . "'";
                    $banresult1 = mysqli_query($sql, $banquery1)
                        or die(mysqli_error($sql));

                    $banquery2 = "INSERT INTO ts_unban_requests (ban_id, link_id, `time`, mcname) VALUES ('" . mysqli_result($bannedr, 0, "ID") . "', '" . mysqli_result($userr, 0, "ID") . "', '" . time() . "', '" . mysqli_result($userr, 0, "minecraft") . "')";
                    $banresult2 = mysqli_query($sql, $banquery2)
                        or die(mysqli_error($sql));

                    $params["banned"] = true;
                    $_SESSION["ban_sessid"] = $updated[1];
                }
            }
        }
        else
            $showform = true;

        $params["showform"] = $showform;
        drawPage2("servers.html", $params);
    }
}
else {
    drawPage2("login.html", array("auth_link" => $twitch->authenticate()));
}
?>

<script>
    var currUpdate = <?php echo $_SESSION["ban_sessid"]; ?>;

    function less() {
        $("#serverMessage").load("api.php", {request: "canplay", last_id: currUpdate});
        setTimeout(less, 1000);
    }

    setTimeout(less, 1000);
</script>
