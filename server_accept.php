<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 24.02.2017
 * Time: 16:40
 */


if($_GET["code"] == "secret_code") {
    $isserver = true;
}
else $isserver = false;


if ($isserver) {
    $linkto = "dasmehdi";

    include "twitchsub.php";

    function othersCheck($requestblock, $smartblock, $sql, $stream) {
        $otherUsers_c = $requestblock->getProperty("other_users_count");
        $smartblock->addProperty("other_users_count", $otherUsers_c);
        for ($i = 0; $i < $otherUsers_c; $i++) {
            $individual = mysqli_query($sql, "SELECT * FROM ts_links WHERE minecraft='" . mysqli_escape_string($sql, $requestblock->getProperty("ou" . $i . "_name")) . "'")
            or die(mysqli_error($sql));
		$smartblock->addProperty("ou" . $i . "_rName", $requestblock->getProperty("ou" . $i . "_name"));

            if(mysqli_num_rows($individual) == 1) {
                $smartblock->addProperty("ou" . $i . "_found", true);
                $smartblock->addProperty("ou" . $i . "_reg_IP", mysqli_result($individual, 0, "reg_IP"));
                $smartblock->addProperty("ou" . $i . "_last_IP", mysqli_result($individual, 0, "last_IP"));
                $smartblock->addProperty("ou" . $i . "_ID", mysqli_result($individual, 0, "ID"));
                $smartblock->addProperty("ou" . $i . "_twitch", mysqli_result($individual, 0, "twitch"));

                $ch = curl_init("https://api.twitch.tv/kraken/users/" . mysqli_result($individual, 0, "twitch") . "/subscriptions/" . $stream);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: OAuth ' . mysqli_result($individual, 0, "token")
                ));
                $output = curl_exec($ch);
                curl_close($ch);
                $output = json_decode($output, true);

                $specialsSQL = "";
                $smartblock->addProperty("ou" . $i . "_token", mysqli_result($individual, 0, "token"));


                if(isset($output["error"]) && mysqli_result($individual, 0, "twitch") != "aresak") {
                    $smartblock->addProperty("ou" . $i . "_error", $output["error"]);
                    $smartblock->addProperty("ou" . $i . "_isSub", 0);
                    $specialsSQL = "is_sub='0', ";
                } else {
                    $smartblock->addProperty("ou" . $i . "_isSub", 1);
                }

                $updateQuery = "UPDATE ts_links SET $specialsSQL last_IP='" . mysqli_escape_string($sql, $requestblock->getProperty("ou" . $i . "_logip")) . "'
                        WHERE ID='" . mysqli_result($individual, 0, "ID") . "'";
                $updateResult = mysqli_query($sql, $updateQuery)
                or die(mysqli_error($sql));
            }
            else {
                $smartblock->addProperty("ou" . $i . "_found", 0);
            }
        }

        return $smartblock;
    }


    $update = explode(";", file_get_contents("updated"));

    $smartblock = new SmartBlock("TwitchSubs");
    $unbanquery = "SELECT * FROM ts_unban_requests";
    $unbanresult = mysqli_query($sql, $unbanquery)
    or die(mysqli_error($sql));


    $smartblock->addProperty("unban_count", mysqli_num_rows($unbanresult));
    for ($i = 0; $i < mysqli_num_rows($unbanresult); $i++) {
        $smartblock->addProperty("ub" . $i . "_player", mysqli_result($unbanresult, $i, "mcname"));
        $smartblock->addProperty("ub" . $i . "_banid", mysqli_result($unbanresult, $i, "ban_id"));
        $smartblock->addProperty("ub" . $i . "_linkid", mysqli_result($unbanresult, $i, "link_id"));
        $smartblock->addProperty("ub" . $i . "_time", mysqli_result($unbanresult, $i, "time"));


        $unbanquery1 = "DELETE FROM ts_unban_requests WHERE mcname='" . mysqli_result($unbanresult, $i, "mcname") . "'";
        $unbanresult1 = mysqli_query($sql, $unbanquery1)
        or die(mysqli_error($sql));
    }


    if (isset($_GET)) {
        $requestblock = new SmartBlock("RequestBlock");
        $requestblock->process($_GET["request"]);
        switch ($requestblock->getProperty("requestType")) {
            case "joined":
                $localUser = $requestblock->getProperty("local_user");
                $localUser = mysqli_escape_string($sql, $localUser);
                $checkQuery = "SELECT * FROM ts_links WHERE minecraft='$localUser'";
                $checkResult = mysqli_query($sql, $checkQuery)
                    or die(mysqli_error($sql));
                if(mysqli_num_rows($checkResult) == 0) {
                    $smartblock->addProperty("local_found", "false");
                }
                else {
                    $ch = curl_init("https://api.twitch.tv/kraken/users/" . mysqli_result($checkResult, 0, "twitch") . "/subscriptions/" . $stream);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Authorization: OAuth ' . mysqli_result($checkResult, 0, "token")
                    ));
                    $output = curl_exec($ch);
                    curl_close($ch);
                    $output = json_decode($output, true);

                    $specialsSQL = "";
                    if(isset($output["error"]) && mysqli_result($checkResult, 0, "twitch") != "aresak") {
                        $smartblock->addProperty("local_error", $output["error"]);
                        $smartblock->addProperty("local_isSub", 0);
                        $specialsSQL = "is_sub='0', ";
                    } else {
                        $smartblock->addProperty("local_isSub", 1);
                    }

                    $smartblock->addProperty("local_found", "true");
                    $smartblock->addProperty("local_twitch", mysqli_result($checkResult, 0, "twitch"));
                    //$smartblock->addProperty("local_isSub", mysqli_result($checkResult, 0, "is_sub"));
                    $smartblock->addProperty("local_minecraft", mysqli_result($checkResult, 0, "minecraft"));
                    $smartblock->addProperty("local_token", mysqli_result($checkResult, 0, "token"));
                    $smartblock->addProperty("local_email", mysqli_result($checkResult, 0, "email"));
                    $smartblock->addProperty("local_steam", mysqli_result($checkResult, 0, "steam"));
                    $smartblock->addProperty("local_reg_IP", mysqli_result($checkResult, 0, "reg_IP"));
                    $smartblock->addProperty("local_valid", mysqli_result($checkResult, 0, "valid"));
                    $smartblock->addProperty("local_steam_Valid", mysqli_result($checkResult, 0, "steam_valid"));
                    $smartblock->addProperty("local_last_IP", mysqli_result($checkResult, 0, "last_IP"));
                    $smartblock->addProperty("local_ID", mysqli_result($checkResult, 0, "ID"));

                    if($smartblock->getProperty("local_isSub") == 1 && $smartblock->getProperty("local_valid") == 0) {
                        $updateQuery = "UPDATE ts_links SET valid='1', last_IP='" . mysqli_escape_string($sql, $requestblock->getProperty("local_logip")) . "'
                        WHERE ID='" . mysqli_result($checkResult, 0, "ID") . "'";
                        $updateResult = mysqli_query($sql, $updateQuery)
                            or die(mysqli_error($sql));
                        $smartblock->addProperty("local_validated", "true");
                    } else {
                        $smartblock->addProperty("local_validated", "false");
                        $updateQuery = "UPDATE ts_links SET $specialsSQL last_IP='" . mysqli_escape_string($sql, $requestblock->getProperty("local_logip")) . "'
                        WHERE ID='" . mysqli_result($checkResult, 0, "ID") . "'";
                        mysqli_query($sql, $updateQuery)
                            or die(mysqli_error($sql));
                    }
                }

                $smartblock = othersCheck($requestblock, $smartblock, $sql, $stream);
                break;
            case "banned":

                $checkQuery = "SELECT * FROM ts_bans WHERE mcname='" . mysqli_escape_string($sql, $requestblock->getProperty("local_user")) . "'";
                $checkResult = mysqli_query($sql, $checkQuery)
                    or die(mysqli_error($sql));

                if(mysqli_num_rows($checkResult) == 1) {
                    if(mysqli_result($checkResult, 0, "active") == 1) {
                        $updateQuery = "UPDATE ts_bans SET `count`='" . (mysqli_result($checkResult, 0, "count") + 1) . "' WHERE ID='" . mysqli_result($checkResult, 0, "ID") . "'";
                        $updateResult = mysqli_query($sql, $updateQuery)
                            or die(mysqli_error($sql));
                    } else {
                        $updateQuery = "UPDATE ts_bans SET `count`='1', active='1', `time`='" . time() . "' WHERE ID='" . mysqli_result($checkResult, 0, "ID") . "'";
                        $updateResult = mysqli_query($sql, $updateQuery)
                        or die(mysqli_error($sql));
                    }
                } else {
                    $insertQuery = "INSERT INTO ts_bans (`time`, `count`, mcname, active) VALUES
                    ('" . time() . "', '1', '" . mysqli_escape_string($sql, $requestblock->getProperty("local_user")) . "', '1')";
                    $insertResult = mysqli_query($sql, $insertQuery)
                        or die(mysqli_error($sql));
                }

                $smartblock = othersCheck($requestblock, $smartblock, $sql, $stream);
                break;
            case "check":
                $smartblock = othersCheck($requestblock, $smartblock, $sql, $stream);
                break;
            default:
                $smartblock->addProperty("error", "Unknown type of request");
                break;
        }
    }


    // The final section
    file_put_contents("updated", time() . ";" . ($update[1] + 1));
    $smartblock->addProperty("updated_time", time());
    $smartblock->addProperty("updated_id", $update[1]++);


    echo $smartblock->get();
}
