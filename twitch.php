<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 23.02.2017
 * Time: 20:40
 */
ini_set('display_errors', 1);
include "twitchtv.php";
$twitch = new TwitchTV();
session_start();

$code = $_GET["code"];


$ch = curl_init("https://api.twitch.tv/kraken/oauth2/token");
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$fields = array(
    'client_id' => "",
    'client_secret' => "",
    'grant_type' => 'authorization_code',
    'redirect_uri' => "",
    'code' => $code
);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
$data     = curl_exec($ch);
$response = json_decode($data, true);


$token = $response["access_token"];
$_SESSION["token"] = $token;
header("Location: index.php?ready");
