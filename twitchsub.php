<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 24.02.2017
 * Time: 16:24
 */

$stream = "dasmehdi";
function mysqli_result($result,$row,$field=0) {
    if ($result===false) return false;
    if ($row>=mysqli_num_rows($result)) return false;
    if (is_string($field) && !(strpos($field,".")===false)) {
        $t_field=explode(".",$field);
        $field=-1;
        $t_fields=mysqli_fetch_fields($result);
        for ($id=0;$id<mysqli_num_fields($result);$id++) {
            if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                $field=$id;
                break;
            }
        }
        if ($field==-1) return false;
    }
    mysqli_data_seek($result,$row);
    $line=mysqli_fetch_array($result);
    return isset($line[$field]) ? $line[$field] : false;
}

session_start();

$sql = mysqli_connect("localhost", "mehdi", "password", "mehdi_sublist");


class SmartBlock {
    public $name = "";
    public $properties = array();

    public $error = "";
    public $isNull = false;

    public function __construct($blockName = "") {
        $this->name = $blockName;
    }

    public function addProperty($name, $value) {
        $this->properties[count($this->properties)] = new SeamlessValue($name, $value);
    }

    public function getProperty($name) {
        foreach($this->properties as $property) {
            if($property->name == $name) {
                return $property->value;
            }
        }
    }

    public function getValue($name) {
        foreach($this->properties as $property) {
            if($property->name == $name) {
                return $property;
            }
        }
    }

    public function get() {
        $result = "";
        $result .= "{";
        $result .= $this->name . ":";

        if($this->properties == null) {
            $result .= "null";
        }
        else {
            for($a = 0; $a < count($this->properties); $a ++) {
                $result .= "[";
                $result .= $this->convertTo($this->properties[$a]->name) . ":" . $this->convertTo($this->properties[$a]->value);
                $result .= "]";
            }
        }

        $result .= "}";
        return $result;
    }

    public function process($input = "") {
        if(empty($input)) {
            $result = "";
            $result .= "{";
            $result .= $this->name . ":";

            if($this->properties == null) {
                $result .= "null";
            }
            else {
                for($a = 0; $a < count($this->properties); $a ++) {
                    $result .= "[";
                    $result .= $this->convertTo($this->properties[$a]->name) . ":" . $this->convertTo($this->properties[$a]->value);
                    $result .= "]";
                }
            }

            $result .= "}";
            return $result;
        }
        else {
            $block = new SmartBlock();
            $blocks = 0;
            $isProperty = false;
            $isValue = false;

            $currentPropertyName = "";
            $currentPropertyValue = "";
            $errorValue = "";


            foreach(str_split($input) as $char) {
                switch($char) {
                    case "{":
                        $blocks++;
                        break;
                    case "}":
                        $blocks--;
                        if (count($errorValue) > 0) {
                            if ($errorValue == "null")
                                $block->isNull = true;
                            else
                                $block->error = $errorValue;
                        }
                        break;
                    case "[":
                        $isProperty = true;
                        break;
                    case "]":
                        $block->addProperty($this->convertFrom($currentPropertyName), $this->convertFrom($currentPropertyValue));

                        $currentPropertyName = "";
                        $currentPropertyValue = "";

                        $isProperty = false;
                        $isValue = false;
                        break;
                    case ":":
                        if ($isProperty)
                            $isValue = true;
                        break;
                    default:
                        if ($isProperty) {
                            if ($isValue) {
                                $currentPropertyValue .= $char;
                            } else {
                                $currentPropertyName .= $char;
                            }
                        } else {
                            if ($isValue) {
                                $errorValue .= $char;
                            } else {
                                $block->name .= $char;
                            }
                        }
                        break;
                    }
                }
            }
            $this->properties = $block->properties;
            $this->name = $block->name;
            $this->error = $block->error;
            $this->isNull = $block->isNull;
        return $block;
    }

    public function convertTo($r) {
        return str_replace(":", "&colon;", $r);
    }

    public function convertFrom($r) {
        return str_replace("&colon;", ":", $r);
    }
}

class SeamlessValue {
    public $name = "";
    public $value = "";

    function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }
}

function drawPage2($page, $params) {
    $table = "";
    if(isset($params["whitelisted_list"])) {
        foreach($params["whitelisted_list"] as $player) {
            $table .= "<tr>";
            $table .= "<td>" . $player["twitch"] . "</td>";
            $table .= "<td>" . $player["steam"] . "</td>";
            $table .= "<td>" . $player["minecraft"] . "</td>";
            $table .= "</tr>";
        }
    }

    $pref = array(
        "{:Twitch-Name:}" => (isset($params["twitch_name"]) ? $params["twitch_name"] : ""),
        "{:Base-Url:}" => "https://twitch.kactus.xyz/dasmehdi/tmpl/",
        "{:Auth-Link:}" => (isset($params["auth_link"]) ? $params["auth_link"] : ""),
        "{:Whitelisted-List:}" => $table,
        "{:MC_INPUT:}" => (isset($params["input_mc"]) ? $params["input_mc"] : ""),
        "{:STEAM_INPUT:}" => (isset($params["input_steam"]) ? $params["input_steam"] : ""),
        "{:FORM_INPUT:}" => (isset($params["input_form"]) ? $params["input_form"] : ""),
        "{:MC_SERVER_MESSAGE:}" => (isset($params["mc_server_message"]) ? $params["mc_server_message"] : "")
    );


    $html = file_get_contents("tmpl/" . $page);
    $html = strtr($html, $pref);

    echo $html;
}

function drawTable($sql) {
    $query = "SELECT * FROM ts_links WHERE (valid='true' OR steam_valid='true') AND is_sub='1'";
    $result = mysqli_query($sql, $query)
        or die(mysqli_error($sql));

    $res = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
        $res[$i]["twitch"] = mysqli_result($result, $i, "twitch");
        $res[$i]["steam"] = mysqli_result($result, $i, "steam");
        $res[$i]["minecraft"] = mysqli_result($result, $i, "minecraft");
    }

    return $res;
}
